from datetime import datetime

from .app import db


class Sensor(db.Model):
    name = db.Column(db.String(100), nullable=False, primary_key=True)

    def __repr__(self):
        return f"sensor {self.name} "


class SensorData(db.Model):
    timestamp = db.Column(db.DateTime, primary_key=True)
    in_ = db.Column(db.Integer)
    out = db.Column(db.Integer)
    name = db.Column(db.String(100), db.ForeignKey("sensor.name"), nullable=False)

    def __repr__(self):
        return f"Sensor {self.name} registered {self.in_} entries and  {self.out} exits \
            at {self.timestamp.strftime('%Y-%m-%d %H:%S')} "
