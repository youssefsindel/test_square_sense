import logging
from datetime import datetime
from typing import Dict, List, Union

import dateutil.parser as parser
from flask import Flask, make_response, render_template, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import func

logging.basicConfig(
    filename="app_logs.txt", filemode="a", datefmt="%H:%M:%S", level=logging.DEBUG
)


logger = logging.getLogger("mainApp")


app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///./sensorData.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

from .models import Sensor, SensorData
from .serializers import RequestSerializer

db.create_all()
db.session.commit()


@app.route("/updateOccupancy", methods=["POST"])
def updateOccupancy():
    """Webhook to update the occupancy of a room"""
    serializer = RequestSerializer(request.json)
    create_sensor(serializer.name)
    add_data_to_sensor(serializer.name, serializer.in_, serializer.out, serializer.ts)
    return make_response("Succes", 201)


@app.route("/sensors/<sensorName>/history")
def history(sensorName: str) -> str:
    """HTML of all the occupancy updates of the sensor

    Parameters
    ----------
    sensorName : str
        name of the sensor (must exist in database)

    Returns
    -------
    str
        _description_
    """

    sensors = [
        str(i) for i in db.session.query(SensorData).filter_by(name=sensorName).order_by(SensorData.timestamp).all()
    ]
    return render_template("sensorData.html", sensorName=sensorName, recordings=sensors)


@app.route("/sensors", methods=["GET"])
def getSensorsList() -> Dict[str, List[str]]:
    """returns the names of all sensors in the DB"""
    sensors = [i[0] for i in db.session.query(Sensor.name).all()]
    return {"sensors": sensors}



@app.route("/sensors/<sensor_name>/occupancy", methods=["GET"])
def getSensorData(sensor_name: str) -> Dict[str, Union[str, int]]:
    """Returns the number of people registered by the sensor since its initialization

    Parameters
    ----------
    sensor_name : str
        name of the sensor

    Returns
    -------
    Dict[str, Union[str, int]]
        dict with the sensor name and number of registered persons in it, if the
        sensor doesn't exist an additional entry in the dict specifies it (to differentiate from empty room)
    """
    atInstant = request.args.get('atInstant', datetime.now().isoformat())
    ts = parser.isoparse(atInstant)
    all_in = (
        db.session.query(func.sum(SensorData.in_))
        .filter(SensorData.name==sensor_name, SensorData.timestamp <= ts)
        .first()[0]
    )
    all_out = (
        db.session.query(func.sum(SensorData.out))
        .filter(SensorData.name==sensor_name, SensorData.timestamp <= ts)
        .first()[0]
    )
    if not all_in:
        return make_response({
            "sensor": sensor_name,
            "inside": 0,
            "info": "this sensor doesn't exist (yet)",
        }, 204)
    return {"sensor": sensor_name, "inside": all_in - all_out}


@app.errorhandler(404)
def not_found(e):
    """Page not found."""
    return make_response(render_template("404.html"), 404)


def create_sensor(sensor_name: str) -> None:
    """Helper function to create and save sensors into the DB

    Parameters
    ----------
    sensor_name : str
        name of the sensor to be created/saved
    """

    try:
        sensor = Sensor(name=sensor_name)
        db.session.add(sensor)
        db.session.commit()

    except IntegrityError as e:
        db.session.rollback()
        logger.error(
            f"\n Integrity error originated from : {e.orig} \n occured in populate"
        )


def add_data_to_sensor(sensor_name: str, nb_in: int, nb_out: int, ts: datetime):
    """adds new entry to the recording database

    Parameters
    ----------
    sensor_name : str
        name of the sensor
    nb_in : int
        number of people entering
    nb_out : int
        number of people exiting
    ts : datetime
        timestamp of the recording
    """
    try:
        sensor_data = SensorData(name=sensor_name, in_=nb_in, out=nb_out, timestamp=ts)
        db.session.add(sensor_data)
        db.session.commit()
    except IntegrityError as e:
        db.session.rollback()
        logger.error(
            f"\n Integrity error originated from : {e.orig} \n occured when adding data to sensor"
        )


if __name__ == "__main__":
    logger.info("starting flask server")
    app.run(debug=True)
