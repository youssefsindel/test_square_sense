from datetime import datetime
from typing import Dict

import dateutil.parser as parser


class RequestSerializer:
    def __init__(self, data: Dict[str, str]):

        self.name = data.get("sensor", "")
        self.ts = parser.isoparse(data.get("ts", datetime.now().isoformat()))
        self.in_ = data.get("in", 0)
        self.out = data.get("out", 0)

    @property
    def diff(self) -> int:
        return self.in_ - self.out

    def __repr__(self) -> str:
        return f"Sensor {self.name} recorded {self.in_} persons entering and {self.out} exiting the room at {self.ts}"
